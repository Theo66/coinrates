package com.example.bazooka.tradeapp;

import android.os.Parcel;
import android.os.Parcelable;

public class Record implements Parcelable {
     double buy;
     double sell;
     String name;
     int comp_buy;
     int comp_sell;


    public Record(double buy, double sell, String name, int comp_buy, int comp_sell) {
        this.buy = buy;
        this.sell = sell;
        this.name = name;
        this.comp_buy = comp_buy;
        this.comp_sell = comp_sell;

    }

    /////////////TO MAKE PARCEABLE AND SAVE BETWEEN INSTANCES/ROTATIONS//////////
    private Record(Parcel in) {
        buy = in.readDouble();
        sell = in.readDouble();
        name = in.readString();
        comp_buy = in.readInt();
        comp_sell = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return  buy + ": " +sell + ": " +name + ": " +comp_buy + ": " +comp_sell  ;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(buy);
        out.writeDouble(sell);
        out.writeString(name);
        out.writeInt(comp_buy);
        out.writeInt(comp_sell);
    }

    public static final Parcelable.Creator<Record> CREATOR = new Parcelable.Creator<Record>() {
        public Record createFromParcel(Parcel in) {
            return new Record(in);
        }

        public Record[] newArray(int size) {
            return new Record[size];
        }
    };
///////MAKE PARCEABLE FOR SAVING BETWEEN INSTANCES/ROTATIONS/////////////

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComp_buy(Integer comp_buy) {
        this.comp_buy = comp_buy;
    }

    public void setComp_sell(Integer comp_sell) {
        this.comp_sell = comp_sell;
    }

////////////////////////////////
    /////////////////////////////
    ///////////////////////////

    public double getBuy() {
        return buy;
    }

    public double getSell() {
        return sell;
    }

    public String getName() {
        return name;
    }

    public int getComp_buy() {
        return comp_buy;
    }

    public int getComp_sell() {
        return comp_sell;
    }

}
