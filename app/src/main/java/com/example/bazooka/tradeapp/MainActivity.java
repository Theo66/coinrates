package com.example.bazooka.tradeapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONTokener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;



public class MainActivity extends Activity {
    public MainActivity mainAct;      //was static
    public CountDownTimer newtimer;
    ListView lv=null;
    CustomAdapter myAdapter=null;
    ArrayList <Record> Results_List = null;

    LinkedHashMap<String,String> currency_pairs = new LinkedHashMap<String,String>();


    protected String JRequest_Builder(LinkedHashSet<String> currencies){
        LinkedHashSet<String> curr=new LinkedHashSet<String>(currencies);
        StringBuilder sb = new StringBuilder("https://forex.1forge.com/1.0.3/quotes?pairs=");
        Iterator iter = curr.iterator();

        while (iter.hasNext()) {
             sb.append(","+ iter.next());
        }
        sb.append("&api_key=rfd5zobn6WvidvjojKIu2iLHC2qITBV3");
        System.out.println(sb.toString());
        return sb.toString();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mainAct=this;

       /////RELOAD AFTER ROTATION////////
        if (savedInstanceState != null && savedInstanceState.containsKey("LISTA")) {
            ArrayList <Record> Lista_values = savedInstanceState.getParcelableArrayList("LISTA");
            if (Lista_values != null) {
                if(myAdapter==null) {
                    myAdapter=new CustomAdapter(mainAct, Lista_values);
                    lv = (ListView) findViewById(R.id.listView);
                    lv.setAdapter(myAdapter);  //Send the data to listviewAdapter
                }
            }
        }////////////////

        final LinkedHashSet<String> currencies = new LinkedHashSet();
        currencies.add("EURUSD");
        currencies.add("USDJPY");
        currencies.add("USDMXN");
        currencies.add("GBPUSD");
        currencies.add("USDCHF");
        currencies.add("AUDUSD");
        currencies.add("USDCAD");
        currencies.add("NZDUSD");
        currencies.add("ZARJPY");
        currencies.add("USDCNH");
        currencies.add("AUDZND");
        currencies.add("GBPAUD");
        currencies.add("USDZAR");
        currencies.add("CADCHF");
        currencies.add("EURAUD");

        if(!isInternetAvailable()){
            Toast.makeText(this, "No Network Connection", Toast.LENGTH_LONG).show();
        }


        newtimer = new CountDownTimer(1000000000, 5000) {    //Pull Server Updates every 5 seconds.
            public void onTick(long millisUntilFinished) {
                new JSONpull().execute(JRequest_Builder(currencies));
            }
            public void onFinish() {

            }
        };



    }

    @Override
    protected void onResume(){
        super.onResume();
        newtimer.start();

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        newtimer.cancel();

    }

    @Override
    protected void onPause(){
        super.onPause();
        newtimer.cancel();

    }


    /////SAVE DATA (in case of rotation for example)/////
    public void onSaveInstanceState(Bundle savedState) {

        super.onSaveInstanceState(savedState);
       // int values = myAdapter.getValues();  //Mporei na xrhsimopoiithei an thelw na krathsw kai to position
        savedState.putParcelableArrayList("LISTA", Results_List);
    }






    private class JSONpull extends AsyncTask<String, Void, ArrayList<Record>> {  //arguments are: Type of params sent , progress and Result

        protected ArrayList<Record> doInBackground(String... params) {

            ArrayList<Record> Results= new ArrayList<Record>();

            try{
                    URL url;
                    url = new URL(params[0]);

                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String inputLine;
                    StringBuilder strb = new StringBuilder();
                    while ((inputLine = reader.readLine()) != null)
                        strb.append(inputLine);
                    reader.close();

                    JSONTokener tokener = new JSONTokener(strb.toString());
                    JSONArray root_array = new JSONArray(tokener);  //root JSON Array
                    for (int i = 0; i < root_array.length(); i++){    //now find the desired fields/values
                        Record rec = new Record((root_array.getJSONObject(i).getDouble("bid")),
                                                (root_array.getJSONObject(i).getDouble("ask")),
                                                (root_array.getJSONObject(i).getString("symbol")),
                                                0,
                                                0);

                      Results.add(rec);
                    }

            }catch(Exception e){
                System.out.println("Problem tokenizing:" + e);
            }
            return Results;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(ArrayList result_list) {  //Runs on GUI thread
            if(myAdapter==null) {
                myAdapter=new CustomAdapter(mainAct, result_list);
                lv = (ListView) findViewById(R.id.listView);

                lv.setAdapter(myAdapter);  //Send the data to listviewAdapter
            }
            else{
                myAdapter.updateData(result_list);  //update adapter's data
            }

            Results_List=result_list; // Results_List is used to save in case of rotation
        }
    }


    public boolean isInternetAvailable() {   //check if we have internet connectivity
        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
}







