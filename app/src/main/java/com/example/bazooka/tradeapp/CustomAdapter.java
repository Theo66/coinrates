package com.example.bazooka.tradeapp;


import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class CustomAdapter extends BaseAdapter{
    ArrayList<Record> resultList=null;
    ArrayList<Record> nextList=null;
    Context context;
    private LayoutInflater inflater=null;

    public CustomAdapter(MainActivity mainActivity, ArrayList<Record> transferList) {
        // TODO Auto-generated constructor stub

        resultList = transferList;
        context= mainActivity;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {         //Orise to plithos twn GRAMMWN pou tha gemisoun tin lista sthn othoni!!
        // TODO Auto-generated method stub
        return (resultList.size());
    }

    @Override
    public Object getItem(int position) {      //fere to kathe antikeimeno pou tha valeis stin kathe grammh, anadromika me metrhth to position
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {     //epistrefei to position tou kathe antikeimenou analoga to R.xml id tou??
        // TODO Auto-generated method stub
        return position;
    }

    public int getValues(){
        return 0;
    }

    public void updateData(ArrayList<Record> transferList){
        nextList=transferList;

        for(int i=0;i<nextList.size();i++) {
            if (nextList.get(i).getBuy() < resultList.get(i).getBuy()) {
                nextList.get(i).setComp_buy(-1);
            } else if (nextList.get(i).getBuy() > resultList.get(i).getBuy()) {
                nextList.get(i).setComp_buy(1);
            } else {
                nextList.get(i).setComp_buy(0);
            }
        }

        for(int i=0;i<nextList.size();i++) {
            if (nextList.get(i).getSell() < resultList.get(i).getSell()) {
                nextList.get(i).setComp_sell(-1);
            } else if (nextList.get(i).getSell() > resultList.get(i).getSell()) {
                nextList.get(i).setComp_sell(1);
            } else {
                nextList.get(i).setComp_sell(0);
            }
        }

        this.notifyDataSetChanged();
    }


    public class Holder
    {
        TextView keimeno;
    }


       @Override
    public View getView(final int position, View convertView, ViewGroup parent) {    //Note: you don't call this method directly, just need to implement it to tell the parent view how to generate the item's view.
        Holder holder=new Holder();
        View rowView;
        if(nextList!=null) {
            resultList = nextList;
        }

        rowView = inflater.inflate(R.layout.list_row, null);


        holder.keimeno = (TextView) rowView.findViewById(R.id.Exchange);
        holder.keimeno.setText(resultList.get(position).getName());  //name of exchange


        holder.keimeno = (TextView) rowView.findViewById(R.id.Sellrate);
        holder.keimeno.setText(Double.toString(resultList.get(position).getSell()));

        if ((resultList.get(position).getComp_sell())==-1)
            holder.keimeno.setBackgroundColor(Color.parseColor("#FFDF0000"));
        else if ((resultList.get(position).getComp_sell())==1) {
            holder.keimeno.setBackgroundColor(Color.parseColor("#FF006D09"));
        }else if ((resultList.get(position).getComp_sell())==0) {
            holder.keimeno.setBackgroundColor(Color.parseColor("#FF4A4444"));
        }


        holder.keimeno = (TextView) rowView.findViewById(R.id.Buyrate);
        holder.keimeno.setText(Double.toString(resultList.get(position).getBuy()));

        if ((resultList.get(position).getComp_buy())==-1)
            holder.keimeno.setBackgroundColor(Color.parseColor("#FFDF0000"));
        else if ((resultList.get(position).getComp_buy())==1) {
            holder.keimeno.setBackgroundColor(Color.parseColor("#FF006D09"));
        }else if ((resultList.get(position).getComp_buy())==0) {
            holder.keimeno.setBackgroundColor(Color.parseColor("#FF4A4444"));
        }


        double spread=Math.abs(resultList.get(position).getBuy()-resultList.get(position).getSell());
        holder.keimeno = (TextView) rowView.findViewById(R.id.Spreadrate);
        holder.keimeno.setText(new DecimalFormat("#.########").format(spread));


        return rowView;
    }

}
